#version 330 core
in vec2 texture_coordinates;
layout(location=0) out vec4 vFragColor;
uniform sampler2D basic_texture;

void main() {
	vec4 texel = texture(basic_texture, texture_coordinates);
	vFragColor = texel;
}
