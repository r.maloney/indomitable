#version 330 core
layout(location = 0) in vec3 vVertex;
layout(location = 2) in vec2 vText;
out vec2 texture_coordinates;
uniform mat4 MVP;
void main() {
	texture_coordinates = vText;
	gl_Position = MVP*vec4(vVertex,1);
}
