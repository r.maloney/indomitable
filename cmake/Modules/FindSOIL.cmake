# - Try to find SOIL
# Once done this will define
#  SOIL_FOUND - System has SOIL
#  SOIL_INCLUDE_DIRS - The SOIL include directories
#  SOIL_LIBRARIES - The libraries needed to use SOIL
#  SOIL_DEFINITIONS - Compiler switches required for using SOIL

find_package(PkgConfig)

find_path(SOIL_INCLUDE_DIR SOIL.h
          HINTS
          ENV SOIL_ROOT
          SOIL_ROOT
          PATH_SUFFIXES
          include
          include/SOIL
          include/soil)

find_library(SOIL_LIBRARY NAMES soil SOIL
             HINTS
             ENV SOIL_ROOT
             SOIL_ROOT
             PATH_SUFFIXES
             lib
             lib64)

set(SOIL_LIBRARIES ${SOIL_LIBRARY} )
set(SOIL_INCLUDE_DIRS ${SOIL_INCLUDE_DIR} )

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set SOIL_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(SOIL  DEFAULT_MSG
                                  SOIL_LIBRARY SOIL_INCLUDE_DIR)

mark_as_advanced(SOIL_INCLUDE_DIR SOIL_LIBRARY )