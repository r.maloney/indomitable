# - Try to find GSL
# Once done this will define
#  GSL_FOUND - System has GSL
#  GSL_INCLUDE_DIRS - The GSL include directories
#  GSL_LIBRARIES - The libraries needed to use GSL
#  GSL_DEFINITIONS - Compiler switches required for using GSL

find_package(PkgConfig)

find_path(GSL_INCLUDE_DIR gsl_math.h
          HINTS
          ENV GSL_ROOT
          GSL_ROOT
          PATH_SUFFIXES
          include
          include/gsl
          include/GSL)

find_library(GSL_LIBRARY NAMES gsl GSL
             HINTS
             ENV GSL_ROOT
             GSL_ROOT
             PATH_SUFFIXES
             lib
             lib64)

find_library(GSLCBLAS_LIBRARY NAMES gslcblas GSLCBLAS
             HINTS
             ENV GSL_ROOT
             GSL_ROOT
             PATH_SUFFIXES
             lib
             lib64)

set(GSL_LIBRARIES ${GSL_LIBRARY} ${GSLCBLAS_LIBRARY})
set(GSL_INCLUDE_DIRS ${GSL_INCLUDE_DIR})

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set GSL_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(GSL DEFAULT_MSG
                                  GSL_LIBRARY GSL_INCLUDE_DIR)

mark_as_advanced(GSL_INCLUDE_DIR GSL_LIBRARY)