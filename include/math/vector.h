#ifndef VECTOR_H
#define VECT0R_H

typedef struct {
    float x;
    float y;
    float z;
} Vector3d;

typedef struct {
    float u;
    float v;
} Vector2d;

//add(const Vector3d* const a, const Vector3d* const b, Vector3d* result);
//subtract(const Vector3d* const a, const Vector3d* const b, Vector3d* result);
//multiply(const Vector3d* const a, const Vector3d* const b, Vector3d* result);
//divide(const Vector3d* const a, const Vector3d* const b, Vector3d* result);
//negate(Vector3d* a);
//float dotProduct(const Vector3d* const a);
//float magnitude(const Vector3d* const a);

#endif //VECTOR_H