#ifndef MATRIX_H
#define MATRIX_H

typedef struct {
    float values[16];
} Matrix4x4;

typedef Matrix4x4* Matrix;
typedef const Matrix4x4* const MatrixReadOnly;

Matrix createMatrix();
void identity(Matrix matrix);
void orthographic(Matrix matrix, float right, float left, float top, float bottom, float far, float near);
void multiply(Matrix result, MatrixReadOnly a, MatrixReadOnly b);
void print(MatrixReadOnly matrix);

#endif //MATRIX_H