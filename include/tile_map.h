#ifndef TILE_MAP_H
#define TILE_MAP_H

typedef struct {
    int width;
    int height;
    void* data;
} TileMap;

TileMap* createTileMap(const char* tileMapFileName);

void destroyTileMap(TileMap* tileMap);

#endif //TILE_MAP_H