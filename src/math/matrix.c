#include <stdlib.h>
#include <math/matrix.h>
#include <stdio.h>

#include "math/matrix.h"


Matrix createMatrix() {
    Matrix matrix = malloc(sizeof(Matrix4x4));
    return matrix;
}

void identity(Matrix matrix) {
    matrix->values[0] = 1;
    matrix->values[1] = 0;
    matrix->values[2] = 0;
    matrix->values[3] = 0;
    matrix->values[4] = 0;
    matrix->values[5] = 1;
    matrix->values[6] = 0;
    matrix->values[7] = 0;
    matrix->values[8] = 0;
    matrix->values[9] = 0;
    matrix->values[10] = 1;
    matrix->values[11] = 0;
    matrix->values[12] = 0;
    matrix->values[13] = 0;
    matrix->values[14] = 0;
    matrix->values[15] = 1;
}

void orthographic(Matrix matrix, float right, float left, float top, float bottom, float far, float near) {
    matrix->values[0] = 2.f/(right - left);
    matrix->values[1] = 0;
    matrix->values[2] = 0;
    matrix->values[3] = 0;
    matrix->values[4] = 0;
    matrix->values[5] = 2.f/(top - bottom);
    matrix->values[6] = 0;
    matrix->values[7] = 0;
    matrix->values[8] = 0;
    matrix->values[9] = 0;
    matrix->values[10] = -(2.f/(far - near));
    matrix->values[11] = 0;
    matrix->values[12] = -((right + left) / (right - left));
    matrix->values[13] = -((top + bottom) / (top - bottom));
    matrix->values[14] = -((far + near) / (far - near));
    matrix->values[15] = 1;
}

void multiply(Matrix result, MatrixReadOnly a, MatrixReadOnly b) {
    //First result column
    result->values[0] = (a->values[0] * b->values[0]) + (a->values[4] * b->values[1]) + (a->values[8] * b->values[2]) + (a->values[12] * b->values[3]);
    result->values[1] = (a->values[1] * b->values[0]) + (a->values[5] * b->values[1]) + (a->values[9] * b->values[2]) + (a->values[13] * b->values[3]);
    result->values[2] = (a->values[2] * b->values[0]) + (a->values[6] * b->values[1]) + (a->values[10] * b->values[2]) + (a->values[14] * b->values[3]);
    result->values[3] = (a->values[3] * b->values[0]) + (a->values[7] * b->values[1]) + (a->values[11] * b->values[2]) + (a->values[15] * b->values[3]);

    //Second result column
    result->values[4] = (a->values[0] * b->values[4]) + (a->values[4] * b->values[5]) + (a->values[8] * b->values[6]) + (a->values[12] * b->values[6]);
    result->values[5] = (a->values[1] * b->values[4]) + (a->values[5] * b->values[5]) + (a->values[9] * b->values[6]) + (a->values[13] * b->values[6]);
    result->values[6] = (a->values[2] * b->values[4]) + (a->values[6] * b->values[5]) + (a->values[10] * b->values[6]) + (a->values[14] * b->values[6]);
    result->values[7] = (a->values[3] * b->values[4]) + (a->values[7] * b->values[5]) + (a->values[11] * b->values[6]) + (a->values[15] * b->values[6]);

    //Third result column
    result->values[8] = (a->values[0] * b->values[8]) + (a->values[4] * b->values[9]) + (a->values[8] * b->values[10]) + (a->values[12] * b->values[11]);
    result->values[9] = (a->values[1] * b->values[8]) + (a->values[5] * b->values[9]) + (a->values[9] * b->values[10]) + (a->values[13] * b->values[11]);
    result->values[10] = (a->values[2] * b->values[8]) + (a->values[6] * b->values[9]) + (a->values[10] * b->values[10]) + (a->values[14] * b->values[11]);
    result->values[11] = (a->values[3] * b->values[8]) + (a->values[7] * b->values[9]) + (a->values[11] * b->values[10]) + (a->values[15] * b->values[11]);

    //Fourth result column
    result->values[12] = (a->values[0] * b->values[12]) + (a->values[4] * b->values[13]) + (a->values[8] * b->values[14]) + (a->values[12] * b->values[15]);
    result->values[13] = (a->values[1] * b->values[12]) + (a->values[5] * b->values[13]) + (a->values[9] * b->values[14]) + (a->values[13] * b->values[15]);
    result->values[14] = (a->values[2] * b->values[12]) + (a->values[6] * b->values[13]) + (a->values[10] * b->values[14]) + (a->values[14] * b->values[15]);
    result->values[15] = (a->values[3] * b->values[12]) + (a->values[7] * b->values[13]) + (a->values[11] * b->values[14]) + (a->values[15] * b->values[15]);
}

void print(MatrixReadOnly matrix) {
    int i,j;

    for (i = 0; i < 4; ++i) {
        printf("%f %f %f %f\n", matrix->values[i], matrix->values[i + 4], matrix->values[i + 8], matrix->values[i + 12]);
    }

    printf("\n");
}