#include <GL/glew.h>
#include <GL/freeglut.h>
#include <assert.h>
#include <math/matrix.h>
#include <math/vector.h>
#include <stdio.h>
#include <util/ext/stb_image.h>

#define GL_CHECK_ERRORS assert(glGetError()== GL_NO_ERROR);

const int WIDTH = 640;
const int HEIGHT = 480;

typedef struct {
    Vector3d position;
    Vector3d color;
    Vector2d texture;
} Vertex;

Vertex vertices[4];
GLushort indices[6];

Matrix projectionMatrix = NULL;
Matrix modelViewMatrix = NULL;
Matrix mvp = NULL;

GLint mvpUniform;
GLuint vertexArrayId;
GLuint verticsVertexBufferId;
GLuint indicesVertexBufferId;
GLuint shaderProgram;
GLuint textureId;

char* loadShaderFromFile(const char* shader);

GLint createShader(GLuint* shader, GLenum shaderType, const char* shaderSource);

GLint checkShaderStatus(GLuint shader);

GLint checkProgramStatus(GLuint program);

void buildShaderProgram();

void loadTexture();

void onClose();

void onResize(int width, int height);

void onRender();

int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
    glutInitContextVersion(3, 3);
    glutInitContextFlags(GLUT_CORE_PROFILE | GLUT_DEBUG);
    glutInitContextProfile(GLUT_FORWARD_COMPATIBLE);
    glutInitWindowSize(WIDTH, HEIGHT);

    glutCreateWindow("Indomitable");

    glewExperimental = GL_TRUE;
    GLenum glewInitResult = glewInit();

    if (GLEW_OK != glewInitResult) {
        fprintf(stderr, "Error encountered when attempting to initialize GLEW, shutting down: %s\n", glewGetErrorString(glewInit()));
        exit(1);
    }

    if (!GLEW_VERSION_3_3) {
        fprintf(stdout, "GLEW does not support OpenGL 3.3\n");
    }

    if (!GLEW_EXT_separate_shader_objects) {
        fprintf(stderr, "No support for GL_ARB_separate_shader_objects");
        exit(1);
    }

    fprintf(stdout, "GLEW version: %s\n", glewGetString(GLEW_VERSION));
    fprintf(stdout, "Vendor: %s\n", glGetString(GL_VENDOR));
    fprintf(stdout, "Renderer: %s\n", glGetString(GL_RENDERER));
    fprintf(stdout, "Version: %s\n", glGetString(GL_VERSION));
    fprintf(stdout, "GLSL: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    vertices[0].color = (Vector3d) {.x = 1, .y = 0, .z = 0};
    vertices[1].color = (Vector3d) {.x = 0, .y = 1, .z = 0};
    vertices[2].color = (Vector3d) {.x = 0, .y = 0, .z = 1};
    vertices[3].color = (Vector3d) {.x = 0, .y = 0, .z = 1};

    vertices[0].position = (Vector3d) {.x = 160, .y = 320, .z = 0};
    vertices[1].position = (Vector3d) {.x = 160, .y = 160, .z = 0};
    vertices[2].position = (Vector3d) {.x = 320, .y = 320, .z = 0};
    vertices[3].position = (Vector3d) {.x = 320, .y = 160, .z = 0};

    vertices[0].texture = (Vector2d) {.u = 0, .v = 0};
    vertices[1].texture = (Vector2d) {.u = 0, .v = 1};
    vertices[2].texture = (Vector2d) {.u = 1, .v = 0};
    vertices[3].texture = (Vector2d) {.u = 1, .v = 1};

    indices[0] = 0;
    indices[1] = 1;
    indices[2] = 2;
    indices[3] = 2;
    indices[4] = 1;
    indices[5] = 3;

    buildShaderProgram();

    loadTexture();

    modelViewMatrix = createMatrix();
    identity(modelViewMatrix);
    projectionMatrix = createMatrix();
    identity(projectionMatrix);
    mvp = createMatrix();
    identity(mvp);

    glutCloseFunc(onClose);
    glutDisplayFunc(onRender);
    glutReshapeFunc(onResize);

    glutMainLoop();

    glDeleteProgram(shaderProgram);
    glDeleteBuffers(1, &verticsVertexBufferId);
    glDeleteBuffers(1, &indicesVertexBufferId);
    glDeleteVertexArrays(1, &vertexArrayId);
}

void buildShaderProgram() {
    GLuint vertexShader;
    char* shaderSource = loadShaderFromFile("resources/shader/shader.vert");
    GLint shaderStatus = createShader(&vertexShader, GL_VERTEX_SHADER, shaderSource);
    assert(GL_FALSE != shaderStatus);
    free(shaderSource);

    GLuint fragmentShader;
    shaderSource = loadShaderFromFile("resources/shader/shader.frag");
    shaderStatus = createShader(&fragmentShader, GL_FRAGMENT_SHADER, shaderSource);
    assert(GL_FALSE != shaderStatus);
    free(shaderSource);

    shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    glLinkProgram(shaderProgram);
    GLint programStatus = checkProgramStatus(shaderProgram);
    assert(GL_FALSE != programStatus);
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    glUseProgram(shaderProgram);
    GLint vertexAttrib = glGetAttribLocation(shaderProgram, "vVertex");
//    GLint colorAttrib = glGetAttribLocation(shaderProgram, "vColor");
    GLint textureAttrib = glGetAttribLocation(shaderProgram, "vText");
    mvpUniform = glGetUniformLocation(shaderProgram, "MVP");
    glUseProgram(0);

    GL_CHECK_ERRORS

    glGenVertexArrays(1, &vertexArrayId);
    glGenBuffers(1, &verticsVertexBufferId);
    glGenBuffers(1, &indicesVertexBufferId);
    glBindVertexArray(vertexArrayId);

    GL_CHECK_ERRORS

    //Bind buffered vertices
    glBindBuffer(GL_ARRAY_BUFFER, verticsVertexBufferId);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), &vertices[0], GL_STATIC_DRAW);

    GL_CHECK_ERRORS

    //Enable vertex attribute
    glEnableVertexAttribArray(vertexAttrib);
    glVertexAttribPointer(vertexAttrib, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);

    GL_CHECK_ERRORS

//    //Enable color attribute
//    glEnableVertexAttribArray(colorAttrib);
//    glVertexAttribPointer(colorAttrib, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*) offsetof(Vertex, color));
//
//    GL_CHECK_ERRORS

    //Enable texture attribute
    glEnableVertexAttribArray(textureAttrib);
    glVertexAttribPointer(textureAttrib, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*) offsetof(Vertex, texture));

    GL_CHECK_ERRORS

    //Bind buffered indices
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesVertexBufferId);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), &indices[0], GL_STATIC_DRAW);

    GL_CHECK_ERRORS
}

void loadTexture() {
    int x,y,n;
    int forceChannels = 4; //RGBA - 4 bytes per pixel
    char* fileName = "resources/sprite/male_default.png";
    unsigned char* imageData = stbi_load(fileName, &x, &y, &n, forceChannels);

    if (!imageData) {
        fprintf(stderr, "An error occurred while opening texture file: %s\n", fileName);
    }

    //Warn for Non POT
    if ( (0 != (x & (x-1))) || (0 != (y & (y-1))) ) {
        fprintf(stderr, "WARNING: Texture %s dimensions are not powers of two: (%d, %d)", fileName, x, y);
    }

    //Flip texture so Y axis 0 is at the bottom of the texture as OpenGL expects
    int textureWidthInBytes = x * forceChannels;
    unsigned char* top = NULL;
    unsigned char* bottom = NULL;
    unsigned char temp = 0;
    int halfHeight = y/2;

    for(int row = 0; row < halfHeight; ++row) {
        //Offset from the top down by ROW
        top = imageData + row * textureWidthInBytes;
        //Offset from the bottom up by ROW
        bottom = imageData + (y - row - 1) * textureWidthInBytes;
        for (int column = 0; column < textureWidthInBytes; ++column) {
            temp = *top;
            *top = *bottom;
            *bottom = temp;
            ++top;
            ++bottom;
        }
    }

    glGenTextures(1, &textureId);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureId);
    glTexImage2D(
            GL_TEXTURE_2D,
            0,
            GL_RGBA,
            x,
            y,
            0,
            GL_RGBA,
            GL_UNSIGNED_BYTE,
            imageData);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);


}

char* loadShaderFromFile(const char* shader) {
    FILE* fp;
    long fileSize;
    char* buffer;
    size_t result;

    fp = fopen(shader, "rb");

    assert(NULL != fp);

    fseek(fp, 0, SEEK_END);
    fileSize = ftell(fp);
    rewind(fp);

    buffer = (char*) malloc(sizeof(char) * fileSize);

    assert(NULL != buffer);

    result = fread(buffer, 1, fileSize, fp);

    assert(fileSize == result);

    fclose(fp);

    buffer[fileSize] = '\0';

    return buffer;
}

GLint createShader(GLuint* shader, GLenum shaderType, const char* shaderSource) {
    *shader = glCreateShader(shaderType);
    glShaderSource(*shader, 1, &shaderSource, NULL);
    glCompileShader(*shader);
    return checkShaderStatus(*shader);
}

GLint checkShaderStatus(GLuint shader) {
    GLint status;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
    if (status == GL_FALSE) {
        GLint infoLogLength;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);
        GLchar infoLog[infoLogLength];
        glGetShaderInfoLog(shader, infoLogLength, NULL, infoLog);
        fprintf(stderr, "Error while compiling shader: %s", infoLog);
    }

    return status;
}

GLint checkProgramStatus(GLuint program) {
    GLint status;
    glGetProgramiv(program, GL_LINK_STATUS, &status);
    if (status == GL_FALSE) {
        GLint infoLogLength;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);
        GLchar infoLog[infoLogLength];
        glGetProgramInfoLog(program, infoLogLength, NULL, infoLog);
        fprintf(stderr, "Error while linking program: %s", infoLog);
    }

    return status;
}

void onRender() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUseProgram(shaderProgram);
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, mvp->values);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);
    glUseProgram(0);

    glutSwapBuffers();
}

void onClose() {
    glDeleteProgram(shaderProgram);
    glDeleteBuffers(1, &verticsVertexBufferId);
    glDeleteBuffers(1, &indicesVertexBufferId);
    glDeleteVertexArrays(1, &vertexArrayId);
}

void onResize(int width, int height) {
    glViewport(0, 0, (GLsizei) width, (GLsizei) height);
    orthographic(projectionMatrix, 1.f * width, 0.f, 1.f * height, 0.f, 1.f, -1.f);
    printf("New projection Matrix\n");
    print(projectionMatrix);

    printf("MVP Before multiply.\n");
    print(mvp);

    multiply(mvp, projectionMatrix, modelViewMatrix);

    printf("MVP After multiply.\n");
    print(mvp);
}
